import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendeesList from './AttendeesList';
import AttendConferenceForm from './AttendConferenceForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="locations/new" element={<LocationForm />} />
        <Route path="conferences/new" element={<ConferenceForm />} />
        <Route path="presentations/new" element={<PresentationForm />} />
        <Route path="attendees">
          <Route index element={<AttendeesList attendees={props.attendees} />} />
          <Route path="new" element={<AttendConferenceForm />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
