function createCard(name, description, pictureUrl, starts, ends, location) {
  const startDate = new Date(starts);
  const endDate = new Date(ends);
  const options = {year: 'numeric', month: '2-digit', day: '2-digits'};
  const startStr = startDate.toLocaleDateString('en-US, options');
  return `
  <div class="col-md-4 md-4">
    <div class="card shadow-lg md-4">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle md-2 text-muted">Card substitle</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
      ${starter} - ${endStr}
      </div>
    </div>
  </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Handle bad response
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log(details);
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const html = createCard(title, description, pictureUrl);
          const column = document.querySelector('.col');
          column.innerHTML += html;


          const conferencesContainer = document.querySelector('.container');
          conferencesContainer.innerHTML += html;
        }
      }
    }
  } catch (e) {

  }
})
